const users = [];


//Add user to Room
const addUser = ({ id, name, room }) => {
    name = name.trim().toLowerCase();
    room = room.trim().toLowerCase();
    //check existing users
    const existingUser = users.find((user) => user.room === room && user.name === name);
    if (existingUser) {
        return { error: 'Username is Taken' }
    }
    const user = { id, name, room };
    //add new user to array
    users.push(user);
    //return new user
    return { user }
}
//Remove User from Room
const removeUser = ({ id }) => {
    const index = users.findIndex((user) => user.id === id);
    if (index !== -1) {
        return users.splice(index, 1)[0];
    }
}
//Get User from Rom
const getUser = (id) => users.find((user) => user.id === id);
//get All User
const getUsersInRoom = (room) => users.filter((user) => user.room === room);

module.exports = {
    addUser, removeUser, getUser, getUsersInRoom
};